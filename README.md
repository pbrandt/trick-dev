# Trick sim

This is a simulation of a rocket that goes straight up and then comes straight down to propulsively land.

This repo represents my first attempt at writing a sim like this, and I'm not super proud of it but I'm making it public just so other people can see it.


# Vagrant

```
vagrant up
```

if something goes wrong during setup, fix bootstrap.sh and run:

```
vagrant reload --provision
```

then to get into your vm:

```
vagrant ssh
```

to run GUI stuff:

```
vagrant ssh -- -Y
```
