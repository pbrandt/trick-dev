#!/bin/sh -e

sudo apt-get update -qq > /dev/null # booooring output

# install the usual tools
sudo apt-get -qqy install build-essential git vim curl virtualenv > /dev/null

# install trick dependencies software
sudo apt-get -qqy install bison curl flex g++ libx11-dev libxml2-dev libxt-dev \
 libmotif-common libmotif-dev make openjdk-8-jdk python2.7-dev swig \
 zlib1g-dev llvm llvm-dev clang libclang-dev libudunits2-dev > /dev/null

# install maaaaaaagic
sudo apt-get -qqy install clang-tidy

# install node.js 9.x
if which node 2>/dev/null; then
  echo node `node --version`
else
  echo installing node
  curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash - > /dev/null
  sudo apt-get install -qqy nodejs > /dev/null
  echo node `node --version`
  echo npm `npm --version`
fi

# install the node dev things
# npm i -g yarn > /dev/null
# npm i -g pm2 > /dev/null
npm i -g gulp-cli > /dev/null
cd /vagrant/cannon_analytic
npm install > /dev/null

# install trick itself
cd /vagrant
if [ ! -d Trick ]; then
  git clone https://github.com/nasa/trick.git Trick
fi
cd Trick
./configure
make
sudo make install

# make the bashrc pretty ok (i prefer fish, but I guess it's best to use bash in shared environments)
sudo ln -sf -T /vagrant/bashrc /home/ubuntu/.bashrc
ln -sf -T /vagrant /home/ubuntu/trick-dev

cd
echo "wow"
echo "such done"
echo 'type "vagrant ssh" to get in'
