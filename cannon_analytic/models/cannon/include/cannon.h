/*************************************************************************
PURPOSE: (Represent the state and initial conditions of a cannonball)
**************************************************************************/
#ifndef CANNON_H
#define CANNON_H

// #include "Eigen/Core"

class Cannon {
public:
  double pos[3]; /* m position */
  double vel[3]; /* m/s velocity */
  double acc[3]; /* m/s2 acceleration */
  double time;

  bool impact; /* -- Has impact occured? */
  double impactTime; /* s Time of Impact */

  Cannon();
  int init();
  int shutdown();
  int derivative();
  int integration();
};

#endif
