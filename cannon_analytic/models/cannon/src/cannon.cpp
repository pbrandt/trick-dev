/******************************* TRICK HEADER ****************************
PURPOSE: (Set the initial data values)
*************************************************************************/

/* Model Include files */
#include <math.h>
#include <stdio.h>
#include <stddef.h>
#include "trick/exec_proto.h"
#include "trick/integrator_c_intf.h"
#include "../include/cannon.h"

/* default data job */
Cannon::Cannon() {
    this->acc[1] = -9.81;
    this->time = 0.0;
    this->impact = 0;
    this->impactTime = 0.0;
}

/* initialization job */
int Cannon::init() {

    double init_angle = 3.14 / 4.0;
    double init_speed = 10.0;

    this->vel[0] = init_speed*cos(init_angle);
    this->vel[1] = init_speed*sin(init_angle);

    this->impactTime = 0.0;
    this->impact = 0.0;

    return 0 ;
}

int Cannon::shutdown() {
    double t = exec_get_sim_time();
    printf( "========================================\n");
    printf( "      Cannon Ball State at Shutdown     \n");
    printf( "t = %g, pos = [%g, %g], vel = [%g, %g]\n", t, this->pos[0], this->pos[1], this->vel[0], this->vel[1] );
    printf( "========================================\n");
    return 0 ;
}

int Cannon::derivative() {
  this->acc[0] = 0.0;
  this->acc[2] = 0.0;
  this->acc[1] = this->pos[1] < 0 ? 0.0 : -9.81; // mars gravity
  return 0 ;
}

int Cannon::integration() {
    int ipass;

    load_state(
        &this->pos[0] ,
        &this->pos[1] ,
        &this->pos[2] ,
        &this->vel[0] ,
        &this->vel[1] ,
        &this->vel[2] ,
        NULL);

    load_deriv(
        &this->vel[0] ,
        &this->vel[1] ,
        &this->vel[2] ,
        &this->acc[0] ,
        &this->acc[1] ,
        &this->acc[2] ,
        NULL);

    ipass = integrate();

    unload_state(
        &this->pos[0] ,
        &this->pos[1] ,
        &this->pos[2] ,
        &this->vel[0] ,
        &this->vel[1] ,
        &this->vel[2] ,
        NULL );

    return(ipass);

}
