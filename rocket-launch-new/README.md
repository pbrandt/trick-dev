# Rocket Ascent Simulation

in this simulation we will model the control loop


# descent control

Assume bang-bang control. or coast-burn control.

## Coast

```
a = -g
v = -g *t + v0
p = -g * t*t / 2 + v0 * t + p0

v_b = velocity when the burn starts
t_b = time that burn starts, measured from t = 0

v_b = -g * t_b + v0
p_b = -g * t_b * t_b + v0 * t_b + p0
```

## Burn

For the burn we'll take t = 0 at the start of the burn, not the start of the coast phase. just makes some things easier.

that makes the landing time t_f equal the duration of the burn.

```
a = -g + F / m(t)

m(t) = m0 - k*t, where k = F/(g * Isp)

v = c1 -g * t - F/k * log(m0 - k*t)
p = c2 + c1 * t - g * t * t / 2 - F/k * [......]

v_b = -F/k * log(m0)
p_b = c2 - F/k * []

final velocity and position is 0
0 = c1 - g * t_f - F/k * log(m0 - k * t_f)
0 = c2 + c1 * t_f -
```

## Solving

to solve, put in 4 equations and 4 unknowns. set all equations equal to 0 and use any root finding algorithm you want. i used newton-raphson. here's the matrix version:

so x is the unknown vector, which is [c1, c2, t_b, t_f]. x_0 is an initial guess, f_0 would be the equations evaluated at that guess. when f_0 is pretty close to 0 you're done. x_i is the next guess.

```
x_i = x0 - J_inverse * f_0
```

i've found this to work pretty well, but for other systems of equations maybe something else would be better. either way, once you have the jacobian, you can use basically any numerical method.
