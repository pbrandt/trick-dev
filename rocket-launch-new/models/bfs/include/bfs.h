/*************************************************************************
   PURPOSE: (Represent the state and initial conditions of BFS)
 **************************************************************************/
#ifndef BFS_H
#define BFS_H

class BFS {

public:
  double acc[3]; /* m/s2 xy-acceleration  */
  double vel[3]; /* m/s xy-velocity */
  double pos[3]; /* m xy-position */
  double q[4]; /* * orientation quaternion, rotation from eci frame */
  double qdot[4]; /* * angular velocity quaternion */

  double cg[3]; /* m center of gravity in body relative coordinate system */
  double cp[3]; /* m center of pressure in body relative coordinate system */

  double payload_mass;  /* kg mass of bfs payload */
  double structure_mass; /* kg mass of bfs rocket itself */
  double propellant_mass; /* kg amount of propellant in the tanks */
  double oxidizer_mass; /* kg amount of oxider in the tanks */

  double oxidizer_mdot; /* kg/s mass rate of change for oxidizer */
  double propellant_mdot; /* kg/s mass rate of change for propellant */

  double isp; /* s current ISP of engines */
  double throttle; /* 1 throttle ratio, betwen 0.5 and 1.0 */
  double max_thrust; /* N maximum thrust */

  double time; /* s Model time */

  double velf[3]; /* m/s landing speed */

  BFS();
  int init();
  int derivative();
  double get_density(double altitude);
  int integration();
  int shutdown();

};

#endif
