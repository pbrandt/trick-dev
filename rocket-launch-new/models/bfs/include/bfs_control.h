/*************************************************************************
PURPOSE: ( BFS Control Model )
**************************************************************************/

#ifndef BFS_CONTROL_H
#define BFS_CONTROL_H

#include "bfs.h"
#include "Eigen/Core"
#include "Eigen/Dense"

enum FLIGHT_PHASE {
  FLIGHT_PHASE_ASCENT,
  FLIGHT_PHASE_COAST,
  FLIGHT_PHASE_BOOSTBACK,
  FLIGHT_PHASE_REENTRY,
  FLIGHT_PHASE_REENTRY_COAST,
  FLIGHT_PHASE_DESCENT,
  FLIGHT_PHASE_LANDING,
  FLIGHT_PHASE_TOUCHDOWN,
  FLIGHT_PHASE_LANDED
};

class BFS_Control {
public:
  double target_apogee; /* m target apogee altitude from geodetic reference, so like 100000 would be space */
  double target_eccentricity; /* * target orbit ecentricity, 0 = circular */
  double target_inclination; /* deg target orbig inclination */
  // we'll target the other things later

  FLIGHT_PHASE flight_phase; /* * current flight phase */

  BFS* bfs; /* * the rocket we are controlling */

  BFS_Control(BFS *bfs);
  void set_target(double a, double e, double i);
  void control();

  void ascent_control();

  void descent_control();
  Eigen::Vector4d descent_f(Eigen::Vector4d guess);
  Eigen::Matrix4d descent_J(Eigen::Vector4d state);
  double descent_ttogo();

  void landing_control();

  void touchdown_control();

};

#endif
