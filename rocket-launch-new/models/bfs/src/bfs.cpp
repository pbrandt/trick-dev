/*********************************************************************
   PURPOSE: ( Trick numeric )
*********************************************************************/
#include "../include/bfs.h"
#include "trick/integrator_c_intf.h"
#include "trick/exec_proto.h"
#include <stddef.h>
#include <stdio.h>
#include "Eigen/Core"

double CD = 0.515;  // close enough
double A = 9.0 * 9.0 / 4.0 * 3.14159;

BFS::BFS() {
  this->payload_mass = 0.0;
  this->structure_mass = 85e3;
  this->oxidizer_mass = 860e3 / 2; // half full
  this->propellant_mass = 240e3 / 2;
  this->isp = 330;
  this->max_thrust = 12.7e6;
}

int BFS::init() {
  return 1;
}

int BFS::shutdown() {
  double t = exec_get_sim_time();
  printf( "========================================\n");
  printf( "          BFS State at Shutdown         \n");
  printf( "t = %g, pos = [%g, %g], vel = [%g, %g]\n", t, this->pos[0], this->pos[1], this->vel[0], this->vel[1] );
  printf( "========================================\n");
  return 1;
}

double BFS::get_density(double altitude) {
  if (altitude < 11000) {
    return 1.2985 + altitude * (0.36539 - 1.2985) / (11000.0 + 610.0);
  } else if (altitude < 20000) {
    return 0.3639 + altitude * (0.088 - 0.3639) / (20000.0 - 11000.0);
  } else if (altitude < 32000) {
    return 0.088 + altitude * (0.0132 - 0.088) / (32000.0 - 20000.0);
  } else if (altitude < 47000) {
    return 0.0132 + altitude * (0.002 - 0.0132) / (47000.0 - 32000.0);
  } else {
    return 0.0;
  }
}

int BFS::derivative() {
  // throttle mdot
  if (this->throttle > 0 && this->oxidizer_mass > 0 && this->propellant_mass > 0) {
    double total_mdot = - this->max_thrust / 9.81 / this->isp * this->throttle;
    this->oxidizer_mdot = total_mdot * 86.0/(86.0 + 24.0);
    this->propellant_mdot = total_mdot * 24.0/(86.0 + 24.0);
  } else {
    // no juice
    this->oxidizer_mdot = 0.0;
    this->propellant_mdot = 0.0;
    this->throttle = 0.0;
  }

  double direction = this->vel[1] > 0 ? 1.0 : -1.0;
  double drag = -1.0 * direction * this->get_density(this->pos[1]) * CD * A * this->vel[1] * this->vel[1] / 2.0;
  double gravity = -9.81;
  double thrust = 12.7e6 * this->throttle; // newtons

  double mass = this->payload_mass + this->structure_mass + this->oxidizer_mass + this->propellant_mass;

  if (this->pos[1] < 0) {
    this->vel[0] = 0.0;
    this->vel[1] = 0.0;
    this->vel[2] = 0.0;
    this->acc[0] = 0.0;
    this->acc[1] = 0.0;
    this->acc[2] = 0.0;
  } else {
    this->acc[1] = gravity + (drag + thrust) / mass;
  }

  return 0;
}

int BFS::integration() {
  int ipass;

  load_state(&this->pos[0], &this->pos[1], &this->pos[2], &this->vel[0], &this->vel[1], &this->vel[2], &this->oxidizer_mass, &this->propellant_mass, NULL);

  load_deriv(&this->vel[0], &this->vel[1], &this->vel[2], &this->acc[0], &this->acc[1], &this->acc[2], &this->oxidizer_mdot, &this->propellant_mdot, NULL);

  ipass = integrate();

  unload_state(&this->pos[0], &this->pos[1], &this->pos[2], &this->vel[0], &this->vel[1], &this->vel[2], &this->oxidizer_mass, &this->propellant_mass, NULL);

  return (ipass);
}
