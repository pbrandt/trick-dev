/*********************************************************************
   PURPOSE: ( BFS control algorithms )
*********************************************************************/
#include "../include/bfs_control.h"
#include "trick/integrator_c_intf.h"
#include <stddef.h>
#include <stdio.h>
#include <iostream>
#include <math.h>


void BFS_Control::control() {
  BFS* bfs = this->bfs;
  // set the next flight phase if neccessary
  if (this->flight_phase == FLIGHT_PHASE_ASCENT && bfs->pos[1] >= 35e3) {
    this->flight_phase = FLIGHT_PHASE_COAST;
  } else if (this->flight_phase == FLIGHT_PHASE_COAST && bfs->vel[1] < -10.0) {
    this->flight_phase = FLIGHT_PHASE_LANDING;
  } else if (this->flight_phase == FLIGHT_PHASE_LANDING && bfs->vel[1] > -10.0) {
    this->flight_phase = FLIGHT_PHASE_TOUCHDOWN;
  } else if (this->flight_phase == FLIGHT_PHASE_TOUCHDOWN && bfs->pos[1] < 0) {
    this->flight_phase = FLIGHT_PHASE_LANDED;
  }

  // perform control based on flight phase
  switch (this->flight_phase) {
    case FLIGHT_PHASE_ASCENT:
      this->ascent_control();
      break;
    case FLIGHT_PHASE_COAST:
      bfs->throttle = 0.0;
      break;
    case FLIGHT_PHASE_LANDING:
      this->landing_control();
      break;
    case FLIGHT_PHASE_TOUCHDOWN:
      this->touchdown_control();
      break;
    case FLIGHT_PHASE_LANDED:
      bfs->throttle = 0.0;
    default:
      bfs->throttle = 0.0;
  }
}

void BFS_Control::ascent_control() {
  this->bfs->throttle = 1.0;
}

void BFS_Control::landing_control() {
  BFS* bfs = this->bfs;
  if (bfs->throttle == 0.0 && this->flight_phase == FLIGHT_PHASE_LANDING) {
    if (this->descent_ttogo() < -2) {
      bfs->throttle = 1.0;
    }
  }
}

/*
determine the time until landing burn should start, for bang-bang control
*/
Eigen::Vector4d BFS_Control::descent_f(Eigen::Vector4d guess) {
  double c1 = guess(0);
  double c2 = guess(1);
  double tb = guess(2);
  double tf = guess(3);

  BFS *bfs = this->bfs;
  double m0 = bfs->structure_mass + bfs->payload_mass + bfs->oxidizer_mass + bfs->propellant_mass;
  double F = bfs->max_thrust;
  double g0 = 9.81;
  double g = 9.81;
  double k = bfs->max_thrust / g0 / bfs->isp;
  double hi = bfs->pos[1];
  double vi = bfs->vel[1];

  Eigen::Vector4d result;
  result(0) = c1 - F / k * log(m0) - (vi - g * tb);
  result(1) = c2 + F * m0 / k / k * log(m0) - (hi + vi * tb - g / 2 * tb * tb);
  result(2) = c1 - g * tf - F / k * log(m0 - k * tf);
  result(3) = c2 + c1 * tf - g / 2 * tf * tf - F / k * ((tf - m0 / k) * log(m0 - k * tf) - tf);

  return result;
}

/**
 * Calculate the jacobian of the system of equations
 * @param  guess [description]
 * @return       [description]
 */
Eigen::Matrix4d BFS_Control::descent_J(Eigen::Vector4d state) {
  double c1 = state(0);
  // double c2 = guess(1);
  double tb = state(2);
  double tf = state(3);

  BFS *bfs = this->bfs;
  double m0 = bfs->structure_mass + bfs->payload_mass + bfs->oxidizer_mass + bfs->propellant_mass;
  double F = bfs->max_thrust;
  double g0 = 9.81;
  double g = 9.81;
  double k = bfs->max_thrust / g0 / bfs->isp;
  // double hi = bfs->pos[1]; unused
  double vi = bfs->vel[1];

  Eigen::Matrix4d jacobian;
  jacobian(0,0) = 1;
  jacobian(0,1) = 0;
  jacobian(0,2) = g;
  jacobian(0,3) = 0;

  jacobian(1,0) = 0;
  jacobian(1,1) = 1;
  jacobian(1,2) = -vi + g * tb;
  jacobian(1,3) = 0;

  jacobian(2,0) = 1;
  jacobian(2,1) = 0;
  jacobian(2,2) = 0;
  jacobian(2,3) = -g + F / (m0 - k * tf);

  jacobian(3,0) = tf;
  jacobian(3,1) = 1;
  jacobian(3,2) = 0;
  jacobian(3,3) = -g * tf - F / k * log(m0 - k * tf) + c1;

  return jacobian;
}

/**
 * Calculate the time to go until the burn commences
 * @return [description]
 */
double BFS_Control::descent_ttogo() {
  /*                              ┌─ c1, unknown integration constant
                                  │       ┌─ c2, unknown integration constant
                                  │       │      ┌─ tb, time until burn starts
                                  │       │      │     ┌─ tf, duration of burn
                                  │       │      │     │                            */
  static Eigen::Vector4d x_guess(4.0e5, -5.0e6, 54.0, 25.0); /* want to use the guess from last time as the first guess next time... so static */

  int max_passes = 20;
  int passes = 0;
  Eigen::Vector4d f_result;
  while (true > 0.1) {
    Eigen::Matrix4d jacobian = this->descent_J(x_guess);
    x_guess = x_guess - jacobian.colPivHouseholderQr().solve(f_result);
    f_result = this->descent_f(x_guess);

    if (f_result.dot(f_result) < 0.1) {
      break;
    }

    passes +=1;
    if (passes > max_passes) {
      // std::cout << "newton raphson failed at height " << bfs->pos[1] << std::endl;
      return 1.0;
      // throw std::runtime_error("Number of newton-raphson passes exceeded");
    }
  }

  return x_guess(2);
}

// softly touch down
void BFS_Control::touchdown_control() {
  BFS *bfs = this->bfs;
  double m = bfs->structure_mass + bfs->payload_mass + bfs->oxidizer_mass + bfs->propellant_mass;
  bfs->throttle = m * 9.81 / bfs->max_thrust;
}
