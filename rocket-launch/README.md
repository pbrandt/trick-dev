# Rocket Ascent Simulation

modeled parts:
- Atmosphere
- rocket body
  - BFR
  - BFS
  - gimbaled thrust
  - drag at all regimes, subsonic, transonic, supersonic
  - staging
  - guidance, targeting to orbit
- gravity
- wind


simulations:
- BFS up and down to crash
- BFS up and landing vertically
- BFS to orbit?
- BFR up and down to crash
- BFR up and landing vertically
- BFR to orbit?
- BFR + BFS
- monte carlo sooner rather than later


