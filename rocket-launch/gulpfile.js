const gulp = require('gulp');
const cp_exec = require('child_process').exec;
require('colors')
var argv = require('yargs').argv

var trk = require('../trick-gui/trk')
var termplot = require('../trick-gui/cli-plot')

/** helper function to run a command **/
async function exec(command, options) {
  return new Promise((resolve, reject) => {
    console.log(`running ${command}`.gray)
    cp_exec(command, options, (e, stdout, stderr) => {
      if (e) {
        console.error(stderr)
        console.error(`error running ${command}: ${e}`)
        reject({
          error: e,
          stdout,
          stderr
        })
      }
      console.log(`finished ${command}`.gray)
      resolve({
        stdout,
        stderr
      })
    })
  })
}

/**
    ,--______
    |  ______|_
    | /       /
    |/_______/
*/
const files = {
  models: 'models/**/*',
  s_define: 'S_define',
  python: 'sims/**/*.py'
}

//
// Tasks
//

// Watch different directories for changes, run appropriately
var watch = module.exports.watch = function() {

  // if source code changes, recompile
  gulp.watch([files.models, files.s_define], async (files) => {
    if (files.path) {
      console.log(`compile triggered by ${files.path}`)
    }
    try {
      await lint(files)
      await compile()
      await run()
      await plot()
    } catch (e) {
      console.error(e)
    }
  })

  // if a python test changes, dom't recompile just run
  gulp.watch(files.python, async (files) => {
    if (files.path) {
      console.log(`run triggered by ${files.path}`)
    }
    run()
  })
}

var lint = module.exports.lint = async function lint(files) {
  var results;
  try {
    if (!files.path) return
    results = await (exec(`clang-tidy ${files.path} -- -Imodels -Ilib`))
  } catch (e) {
    console.log('stout:')
    console.log(e.stdout)
    console.log('stderr:')
    console.log(e.stderr)
    console.log('error')
    console.log(e.error)
    throw('Lint step failed')
  }

  if (results.stderr) {
    console.log(results.stderr)
    console.log(results.stdout)
    throw('Lint step failed')
  }
}

var compile = module.exports.compile = async function compile() {
  try {
    const results = await exec('trick-CP')
  } catch (e) {
    console.log(e.stdout)
    throw("Compile step failed")
  }
}
var build = module.exports.build = module.exports.compile;

var run = module.exports.run = async function run() {
  var sim = argv.sim || __dirname + "/sims/00_expendable_bfs/input.py"
  const results = await exec(`./S_main_*.exe ${sim}`)
  console.log(results.stdout)
}

var plot = module.exports.plot = async function plot() {
  var f = await trk.load(__dirname + '/sims/00_expendable_bfs/log_bfs.trk')
  await termplot(f.data, 'sys.exec.out.time', 'dyn.bfs.pos[1]')
  await termplot(f.data, 'sys.exec.out.time', 'dyn.bfs.oxidizer_mass', 'dyn.bfs.throttle')
  await termplot(f.data, 'sys.exec.out.time', 'dyn.bfs.vel[1]')
}
