# Big Fucking Spaceship

![diagram]()

**stats table**

|  |  |
| :------------- | :------------- |
| Diameter | 9 m |
| Length  | 48 m  |
| Liftoff mass   | 1,335,000 kg  |
| Propellant - CH4  | 240,000 kg  |
| Propellant - O2   | 860,000 kg  |
| Empty weight   | 85,000 kg  |
| Engines   | 3 SL and 4 vacuum Raptors  |
| SL Raptor thrust   | 1.7 MN  |
| SL Raptor Isp sea-level | 330 s  |
| SL Raptor Isp in vacuum   | 356 s |
| SL Raptor Throttle   | 2:1  |
| Vacuum Raptor thrust   | 1.9 MN  |
| Vacuum Raptor Isp sea-level   | -  |
| Vacuum Raptor Isp in vacuuum   | 375 s  |
| Thrust   | 12.7 MN total |

# Tests

If you keep the BFS empty and only fill up the tanks halfway, you can get a thrust to weight ratio of about 2 and have enough fuel for about 140 seconds of full-throttle burn, which should be good enough for testing.
