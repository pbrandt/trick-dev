/*************************************************************************
   PURPOSE: (Represent the state and initial conditions of BFS)
 **************************************************************************/
#ifndef BFS_H
#define BFS_H

enum FLIGHT_PHASE {
  FLIGHT_PHASE_ASCENT,
  FLIGHT_PHASE_COAST,
  FLIGHT_PHASE_BOOSTBACK,
  FLIGHT_PHASE_REENTRY,
  FLIGHT_PHASE_REENTRY_COAST,
  FLIGHT_PHASE_LANDING,
  FLIGHT_PHASE_TOUCHDOWN,
  FLIGHT_PHASE_LANDED
};

typedef struct {

  double vel0[3];  /* *i m Init velocity of BFS */
  double pos0[3];  /* *i m Init position of BFS */
  double q0[4];    /* *i */
  double qdot0[4]; /* *i */

  double acc[3]; /* m/s2 xy-acceleration  */
  double vel[3]; /* m/s xy-velocity */
  double pos[3]; /* m xy-position */
  double q[4];
  double qdot[4]; /* */

  double payload_mass;  /* kg mass of bfs payload */
  double structure_mass; /* kg mass of bfs rocket itself */
  double oxidizer_mass; /* kg amount of oxider in the tanks */
  double propellant_mass; /* kg amount of propellant in the tanks */

  double oxidizer_mdot; /* kg/s mass rate of change for oxidizer */
  double propellant_mdot; /* kg/s mass rate of change for propellant */

  double cg[3]; /* m center of gravity in body relative coordinate system */

  double isp; /* s current ISP of engines */
  double throttle; /* 1 throttle ratio, betwen 0.5 and 1.0 */
  double max_thrust; /* N maximum thrust */

  double time; /* s Model time */
  enum FLIGHT_PHASE flight_phase; /* 1 flight phase */
  int flight_phase_out; /* 1 to plot the flight phase */

  double velf[3]; /* m/s landing speed */

} BFS;

#ifdef __cplusplus
extern "C" {
#endif
int bfs_default_data(BFS *);
int bfs_init(BFS *);
int bfs_shutdown(BFS *);
#ifdef __cplusplus
}
#endif

#endif
