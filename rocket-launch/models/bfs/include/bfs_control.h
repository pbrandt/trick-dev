/*************************************************************************
PURPOSE: ( BFS Control Model )
**************************************************************************/

#ifndef BFS_CONTROL_H
#define BFS_CONTROL_H

#include "bfs.h"

#ifdef __cplusplus
extern "C" {
#endif
int bfs_control(BFS*);
#ifdef __cplusplus
}
#endif
#endif
