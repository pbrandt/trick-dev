/*************************************************************************
PURPOSE: ( BFS Numeric Model )
**************************************************************************/

#ifndef BFS_NUMERIC_H
#define BFS_NUMERIC_H

#include "bfs.h"

#ifdef __cplusplus
extern "C" {
#endif
int bfs_integ(BFS*) ;
int bfs_deriv(BFS*) ;
#ifdef __cplusplus
}
#endif
#endif
