/******************************* TRICK HEADER ****************************
PURPOSE: (Set the initial data values)
*************************************************************************/

/* Model Include files */
#include "../include/bfs.h"
#include <math.h>

/* default data job */
int bfs_default_data(BFS *bfs) {

  bfs->acc[0] = 0.0;
  bfs->acc[1] = 0.0;
  bfs->acc[2] = 0.0;
  bfs->pos0[0] = 0.0;
  bfs->pos0[1] = 0.0;
  bfs->pos0[2] = 0.0;

  bfs->time = 0.0;

  bfs->payload_mass = 0.0;
  bfs->structure_mass = 85e3;
  bfs->oxidizer_mass = 860e3 / 2; // half full
  bfs->propellant_mass = 240e3 / 2;

  bfs->oxidizer_mdot = 0.0;
  bfs->propellant_mdot = 0.0;

  bfs->flight_phase = FLIGHT_PHASE_ASCENT;
  bfs->flight_phase_out = (int)bfs->flight_phase;

  bfs->isp = 330;
  bfs->throttle = 0.0;
  bfs->max_thrust = 12.7e6;

  return 0;
}

/* initialization job */
int bfs_init(BFS *bfs) {

  bfs->vel0[0] = 0;
  bfs->vel0[1] = 0;
  bfs->vel0[2] = 0;

  bfs->vel[0] = bfs->vel0[0];
  bfs->vel[1] = bfs->vel0[1];
  bfs->vel[2] = bfs->vel0[2];

  // bfs->impactTime = 0.0;
  // bfs->impact = 0.0;

  return 0;
}
