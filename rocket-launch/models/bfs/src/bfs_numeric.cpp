/*********************************************************************
   PURPOSE: ( Trick numeric )
*********************************************************************/
#include "../include/bfs_numeric.h"
#include "../include/bfs_control.h"
#include "trick/integrator_c_intf.h"
#include <stddef.h>
#include <stdio.h>
#include "Eigen/Core"

double CD = 0.515;  // close enough
double A = 9.0 * 9.0 / 4.0 * 3.14159;

double get_density(double alt) {
  if (alt < 11000) {
    return 1.2985 + alt * (0.36539 - 1.2985) / (11000.0 + 610.0);
  } else if (alt < 20000) {
    return 0.3639 + alt * (0.088 - 0.3639) / (20000.0 - 11000.0);
  } else if (alt < 32000) {
    return 0.088 + alt * (0.0132 - 0.088) / (32000.0 - 20000.0);
  } else if (alt < 47000) {
    return 0.0132 + alt * (0.002 - 0.0132) / (47000.0 - 32000.0);
  } else {
    return 0.0;
  }
}

int bfs_deriv(BFS *bfs) {
  bfs_control(bfs);

  // throttle mdot
  if (bfs->throttle > 0 && bfs->oxidizer_mass > 0 && bfs->propellant_mass > 0) {
    double total_mdot = - bfs->max_thrust / 9.81 / bfs->isp * bfs->throttle;
    bfs->oxidizer_mdot = total_mdot * 86.0/(86.0 + 24.0);
    bfs->propellant_mdot = total_mdot * 24.0/(86.0 + 24.0);
  } else {
    // no juice
    bfs->oxidizer_mdot = 0.0;
    bfs->propellant_mdot = 0.0;
    bfs->throttle = 0.0;
  }

  double direction = bfs->vel[1] > 0 ? 1.0 : -1.0;
  double drag = -1.0 * direction * get_density(bfs->pos[1]) * CD * A * bfs->vel[1] * bfs->vel[1] / 2.0;
  double gravity = -9.81;
  double thrust = 12.7e6 * bfs->throttle; // newtons

  double mass = bfs->payload_mass + bfs->structure_mass + bfs->oxidizer_mass + bfs->propellant_mass;

  if (bfs->flight_phase == FLIGHT_PHASE_LANDED) {;
    bfs->vel[0] = 0.0;
    bfs->vel[1] = 0.0;
    bfs->vel[2] = 0.0;
    bfs->acc[0] = 0.0;
    bfs->acc[1] = 0.0;
    bfs->acc[2] = 0.0;
  } else {
    bfs->acc[1] = gravity + (drag + thrust) / mass;
  }

  return 0;
}

int bfs_integ(BFS *bfs) {
  int ipass;

  load_state(&bfs->pos[0], &bfs->pos[1], &bfs->pos[2], &bfs->vel[0], &bfs->vel[1], &bfs->vel[2], &bfs->oxidizer_mass, &bfs->propellant_mass, NULL);

  load_deriv(&bfs->vel[0], &bfs->vel[1], &bfs->vel[2], &bfs->acc[0], &bfs->acc[1], &bfs->acc[2], &bfs->oxidizer_mdot, &bfs->propellant_mdot, NULL);

  ipass = integrate();

  unload_state(&bfs->pos[0], &bfs->pos[1], &bfs->pos[2], &bfs->vel[0], &bfs->vel[1], &bfs->vel[2], &bfs->oxidizer_mass, &bfs->propellant_mass, NULL);

  return (ipass);
}
