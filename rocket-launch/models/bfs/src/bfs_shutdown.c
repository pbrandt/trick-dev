/************************************************************************
PURPOSE: (Print the final bfs ball state.)
*************************************************************************/
#include <stdio.h>
#include "../include/bfs.h"
#include "trick/exec_proto.h"

int bfs_shutdown( BFS* bfs) {
    double t = exec_get_sim_time();
    printf( "========================================\n");
    printf( "          BFS State at Shutdown         \n");
    printf( "t = %g, pos = [%g, %g], vel = [%g, %g]\n", t, bfs->pos[0], bfs->pos[1], bfs->vel[0], bfs->vel[1] );
    printf( "========================================\n");
    // bfs->impactTime = 0.0;
    // bfs->impact = 0.0;
    return 0 ;
}
