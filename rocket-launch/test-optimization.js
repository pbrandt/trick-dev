var m = require('simplematrix');

const hi = 21948.9
const vi = -0.0293354
const m0 = 468618
const F = 12.7e6;
const g0 = 9.81;
const g = 9.81;
const k = F / g0 / 330
const log = Math.log.bind(Math);

console.log(Math.exp(-vi * k / F))
console.log((Math.exp(-vi * k / F) - m0) / k)

const tb = 100;
const c1 = vi + F / k * log(m0 - k * tb)
console.log(c1)
console.log(-c1 * tb + hi + vi * tb - F * (m0 - k * tb) * log(m0 - k * tb) - F / k * tb)

// process.exit(0)
// debugger;
//
// c1 : unknonwn integration constant
// c2 : unknown integration constant
// tb : time-until-burn, freefall duration
// tf : burn duration
//

function f(x) {
  var c1 = x[0][0];
  var c2 = x[1][0];
  var tb = x[2][0];
  var tf = x[3][0];

  var result = ([[0], [0], [0], [0]])
  result[0][0] = c1 - F / k * log(m0) - (vi - g * tb);
  result[1][0] = c2 + F * m0 / k / k * log(m0) - (hi + vi * tb - g / 2 * tb * tb);
  result[2][0] = c1 - g * tf - F / k * log(m0 - k * tf);
  result[3][0] = c2 + c1 * tf - g / 2 * tf * tf - F / k * ((tf - m0 / k) * log(m0 - k * tf) - tf);

  return new m.Matrix(result);
}

function J(x) {
  var c1 = x[0][0];
  var c2 = x[1][0];
  var tb = x[2][0];
  var tf = x[3][0];

  var jacobian = ([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
  jacobian[0][0] = 1;
  jacobian[0][1] = 0;
  jacobian[0][2] = g;
  jacobian[0][3] = 0;

  jacobian[1][0] = 0;
  jacobian[1][1] = 1;
  jacobian[1][2] = -vi + g * tb;
  jacobian[1][3] = 0;

  jacobian[2][0] = 1;
  jacobian[2][1] = 0;
  jacobian[2][2] = 0;
  jacobian[2][3] = -g + F / (m0 - k * tf);

  jacobian[3][0] = tf;
  jacobian[3][1] = 1;
  jacobian[3][2] = 0;
  jacobian[3][3] = -g * tf - F / k * log(m0 - k * tf) + c1;

  return new m.Matrix(jacobian);
}

var passes = 0;
var max_passes = 3;
var guess = new m.Matrix([[0], [0], [10], [20]]);
var f_result = f(guess)

while (true) {
  console.log('guess: ', guess)
  console.log('f_result: ', f_result)
  var jacobian = J(guess);
  var addition = jacobian.inverse().times(f_result);
  console.log('addition:', addition)
  guess = guess.plus(jacobian.inverse().times(f_result).times(-1));
  var f_result = f(guess);

  if (f_result[0]*f_result[0] + f_result[1] * f_result[1] + f_result[2] * f_result[2] < 0.1) {
    break;
  }

  passes++;
  if (passes > max_passes) {
    throw new Error("Number of newton-raphson passes exceeded");
  }

}
console.log(f_result)
console.log(guess)
