var trk = require('./trk.js')
var _ = require('lodash')
require('colors')

var line_colors = [
  'green', 'cyan', 'magenta', 'yellow'
]

var plot = module.exports = async function plot(series, x /*, y1, y2, y3... */) {
  var y = Array.prototype.slice.call(arguments, 2)

  var columns = process.stdout.columns
  var height = 25;

  // pre-process the x-axis data
  var x0 = series[0][x]
  var xf = series[series.length - 1][x]
  var xspan = xf - x0
  var dx = xspan / (columns - 7 - 1)

  var tox = function(value) {
    return Math.floor((value - x0) / dx)
  }

  // pre-process the y-axis data
  var lines = y.map((y, i) => {
    var line = {
      name: y,
      color: line_colors[i],
      max: _.max(series.map(p => p[y])),
      min: _.min(series.map(p => p[y]))
    }

    line.dy = (line.max - line.min) / (height - 1)

    line.y = function(value) {
      return Math.floor((value - line.min) / line.dy)
    }
    return line
  })

  // create a graph grid to represent all the available cells
  var graph = new Array(height)
  for (var i = 0; i < graph.length; i++) {
    graph[i] = new Array(columns - 7)
  }


  // plot the lines in the graph grid
  series.map(point => {
    var x_coord = tox(point[x])
    lines.map(line => {
      var y_coord = line.y(point[line.name])
      if (!graph[y_coord]) return;
      graph[y_coord][x_coord] = '─'[line.color]
    })
  })



  // render the graph grid
  console.log(new Array(columns).join('─').gray)

  console.log(lines.map(l => {
    return (' ────'[l.color] + ' ' + l.name)
  }).join('\n'))

  for (var j = height - 1; j >= 0; j--) {
    line = graph[j];
    var chars = []
    for (var i = 0; i < line.length; i++) {
      if (!line[i]) {
        chars.push(' ')
      } else {
        chars.push(line[i])
      }
    }

    // write a y-axis mark
    if (j % 6 === 0) {
      var y_coord = lines[0].min + lines[0].dy * j
      y_coord = y_coord.toExponential(2).replace('+', '')
      console.log(('       ' + y_coord).slice(-7) + chars.join(''))
    } else {
      console.log('       ' + chars.join(''))
    }
  }

  console.log(x_axis(x0, xf, columns, x))



  var separator = new Array(columns)
  for (var i = 0; i < separator.length; i++) {
    separator[i] = '─';
  }
  console.log(separator.join('').gray)
}

function x_axis(x0, xf, cols, name) {
  var axis = new Array(cols - 7)
  axis[0] = '┌'
  for (var i = 1; i < cols-7; i++) {
    if (i % 8 === 0) {
      axis[i] = '┬'
    } else {
      axis[i] = '─'
    }
  }
  axis = '       ' + axis.join('').gray + '\n'

  var label = ('        ' + x0).slice(-8)
  axis += label

  for (var i = 0; i < (cols - 7) / 16 - 1; i++) {
    axis += ('                ' + (x0 + (i+1) * 16 * (xf - x0) / (cols - 7)).toFixed(1)).slice(-16)
  }

  axis += '\n       '
  for (var i = 8; i < (cols - name.length) / 2; i++) axis += ' '
  axis += name

  return axis
}


if (!module.parent) {
  async function main() {
    console.log('running test')
    var f = await trk.load('./log_such_cannon.trk')
    console.log('loaded data')
    console.log(f.data[0])
    plot(f.data, 'sys.exec.out.time', 'dyn.cannon.pos[0]', 'dyn.cannon.pos[1]')
  }
  main().catch(console.error.bind(console))
}
